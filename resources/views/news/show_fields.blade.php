<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $news->user_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $news->title }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $news->slug }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $news->description }}</p>
</div>

<!-- Keywords Field -->
<div class="form-group">
    {!! Form::label('keywords', 'Keywords:') !!}
    <p>{{ $news->keywords }}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    <p>{{ $news->content }}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $news->type }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $news->status }}</p>
</div>

<!-- Image Full Field -->
<div class="form-group">
    {!! Form::label('image_full', 'Image Full:') !!}
    <p>{{ $news->image_full }}</p>
</div>

<!-- Image Preview Field -->
<div class="form-group">
    {!! Form::label('image_preview', 'Image Preview:') !!}
    <p>{{ $news->image_preview }}</p>
</div>

<!-- Banner Big Field -->
<div class="form-group">
    {!! Form::label('banner_big', 'Banner Big:') !!}
    <p>{{ $news->banner_big }}</p>
</div>

<!-- Banner Small Field -->
<div class="form-group">
    {!! Form::label('banner_small', 'Banner Small:') !!}
    <p>{{ $news->banner_small }}</p>
</div>

