<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Keywords Field -->
<div class="form-group col-sm-6">
    {!! Form::label('keywords', 'Keywords:') !!}
    {!! Form::text('keywords', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::number('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::number('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Full Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_full', 'Image Full:') !!}
    {!! Form::text('image_full', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Preview Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_preview', 'Image Preview:') !!}
    {!! Form::text('image_preview', null, ['class' => 'form-control']) !!}
</div>

<!-- Banner Big Field -->
<div class="form-group col-sm-6">
    {!! Form::label('banner_big', 'Banner Big:') !!}
    {!! Form::text('banner_big', null, ['class' => 'form-control']) !!}
</div>

<!-- Banner Small Field -->
<div class="form-group col-sm-6">
    {!! Form::label('banner_small', 'Banner Small:') !!}
    {!! Form::text('banner_small', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('news.index') }}" class="btn btn-default">Cancel</a>
</div>
