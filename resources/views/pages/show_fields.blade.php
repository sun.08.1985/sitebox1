<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $pages->user_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $pages->title }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $pages->slug }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $pages->description }}</p>
</div>

<!-- Keywords Field -->
<div class="form-group">
    {!! Form::label('keywords', 'Keywords:') !!}
    <p>{{ $pages->keywords }}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    <p>{{ $pages->content }}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $pages->type }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $pages->status }}</p>
</div>

<!-- Image Full Field -->
<div class="form-group">
    {!! Form::label('image_full', 'Image Full:') !!}
    <p>{{ $pages->image_full }}</p>
</div>

<!-- Image Preview Field -->
<div class="form-group">
    {!! Form::label('image_preview', 'Image Preview:') !!}
    <p>{{ $pages->image_preview }}</p>
</div>

<!-- Banner Big Field -->
<div class="form-group">
    {!! Form::label('banner_big', 'Banner Big:') !!}
    <p>{{ $pages->banner_big }}</p>
</div>

<!-- Banner Small Field -->
<div class="form-group">
    {!! Form::label('banner_small', 'Banner Small:') !!}
    <p>{{ $pages->banner_small }}</p>
</div>

