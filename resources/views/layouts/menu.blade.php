<li class="{{ Request::is('pages*') ? 'active' : '' }}">
    <a href="{{ route('pages.index') }}"><i class="fa fa-edit"></i><span>Pages</span></a>
</li>

<li class="{{ Request::is('news*') ? 'active' : '' }}">
    <a href="{{ route('news.index') }}"><i class="fa fa-edit"></i><span>News</span></a>
</li>

