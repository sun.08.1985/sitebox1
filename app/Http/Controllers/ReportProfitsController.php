<?php

namespace App\Http\Controllers;
use Yajra\Datatables\Datatables;

class ReportProfitsController extends Controller
{
    public function data()
    {
        return Datatables::of(User::select('*'))->make(true);
    }
}
