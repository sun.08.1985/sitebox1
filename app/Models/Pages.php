<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pages
 * @package App\Models
 * @version January 31, 2020, 2:04 pm UTC
 *
 * @property integer user_id
 * @property string title
 * @property string slug
 * @property string description
 * @property string keywords
 * @property string content
 * @property integer type
 * @property integer status
 * @property string image_full
 * @property string image_preview
 * @property string banner_big
 * @property string banner_small
 */
class Pages extends Model
{
    use SoftDeletes;

    public $table = 'pages';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'title',
        'slug',
        'description',
        'keywords',
        'content',
        'type',
        'status',
        'image_full',
        'image_preview',
        'banner_big',
        'banner_small'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'keywords' => 'string',
        'content' => 'string',
        'type' => 'integer',
        'status' => 'integer',
        'image_full' => 'string',
        'image_preview' => 'string',
        'banner_big' => 'string',
        'banner_small' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'title' => 'required',
        'slug' => 'required',
        'description' => 'required',
        'keywords' => 'required',
        'content' => 'required',
        'type' => 'required',
        'status' => 'required',
        'image_full' => 'required',
        'image_preview' => 'required',
        'banner_big' => 'required',
        'banner_small' => 'required'
    ];


}
