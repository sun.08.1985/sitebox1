<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\News;
use Faker\Generator as Faker;

$factory->define(News::class, function (Faker $faker) {

    return [
        'user_id' => $faker->randomDigitNotNull,
        'title' => $faker->word,
        'slug' => $faker->word,
        'description' => $faker->word,
        'keywords' => $faker->word,
        'content' => $faker->text,
        'type' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'image_full' => $faker->word,
        'image_preview' => $faker->word,
        'banner_big' => $faker->word,
        'banner_small' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
