<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//	if (!session('status'))
//        return view('auth.login');
	return view('welcome');
});

Auth::routes();

Auth::routes(['verify' => true]);

Route::get('/admin', 'AdminController@index')->middleware('verified');

Route::get('/admin/pages', 'PagesController@index')->middleware('verified');
Route::get('/admin/news', 'NewsController@index')->middleware('verified');

Route::resource('/admin/pages', 'PagesController')->middleware('verified');
Route::resource('/admin/news', 'NewsController')->middleware('verified');

